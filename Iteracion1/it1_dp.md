## Diseño orientado a objetos

![Diagrama de Clases](diagramaI1/DiagramaClases.jpg)

## Backlog de iteración

* Como Secretaria/o quiero crear un Paciente para registrar los datos personales.
* Como Secretaria/o quiero crear un Turno para agendar los horarios de atención del Médico.
* Como Secretaria/o quiero confirmar un turno para informar las atenciones que se van a realizar.

## Tareas
* Crear las tablas pacientes, turnos y estados en la Base de Datos
* Crear los modelos Paciente, Turno y Estado con sus respectivos atributos y relaciones con las demás tablas
* Crear seeders para datos de prueba
* Crear controladores PacienteController y TurnoController
* Crear los métodos en los controladores
* Crear rutas
* Crear vistas