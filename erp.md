## Visión

El sistema permitirá informatizar la administración y organización de los Turnos para un Consultorio Médico. Los Turnos son asignados en una fecha y hora, a un paciente determinado.

### Cliente

El sistema es desarrollado para un Médico de la localidad de San José Misiones.

## Lista de características

* El sistema deberá permitir registrar los datos de un Paciente.
* El sistema deberá permitir asignar un Barrio a un Paciente.
* El sistema deberá permitir asignar una Obra Social a un Paciente.
* El sistema deberá permitir registrar un Turno.
* El sistema deberá permitir confirmar un Turno.
* El sistema deberá permitir asignar un Tipo de atención a un Turno.
* El sistema deberá permitir asignar un Paciente a un Turno.
* El sistema deberá permitir registrar un Barrio.
* El sistema deberá permitir registrar una Obra Social.
* El sistema deberá permitir registrar un Tipo de Atención.
* El sistema deberá permitir registrar un Tipo de Teléfono.
* El sistema deberá permitir modificar los datos de un Paciente.
* El sistema deberá permitir modificar los datos de un Barrio.
* El sistema deberá permitir modificar los datos de una Obra Social.
* El sistema deberá permitir modificar los datos de un Tipo de Atención.
* El sistema deberá permitir modificar los datos de un Tipo de Teléfono.
* El sistema deberá permitir eliminar los datos de una Obra Social.
* El sistema deberá permitir filtrar por fecha, Estado y Tipo de Atención los Turnos y generar un reporte en PDF.
* El sistema deberá permitir envíar un correo electrónico al Paciente con los datos del Turno asignado.
* El sistema deberá permitir cancelar (manual o automáticamente) un Turno que no fué confirmado un día antes de la fecha programada.  

## Tecnología a utilizar

* Framework/Lenguaje: Laravel (Versión 5.8) - PHP
* Versionado: GitLab
* Motor de Base de Dato: MySQL
* Documentación: Markdown
* Otras herramientas: Bootstrap (Versión 4.3) - Datatables
* Otros lenguajes: JavaScript
* Arquitectura: Cliente/Servidor
* Otras herramientas: Mailtrap (envío de correos electrónicos), Dompdf (generación de reportes PDF), FullCalendar (calendario)


## Bocetos de interfaces de usuario

![Principal](vistas/Principal.jpg)
![Inicio Sesión](vistas/InicioSesion.jpg)
![Vista Home](vistas/VistaHome.jpg)
![Index Turnos](vistas/IndexTurnos.jpg)
![Confirmar Turno](vistas/ConfirmarTurno.jpg)
![Cancelar Turno](vistas/CancelarTurno.jpg)
![Index Turnos Médico](vistas/IndexTurnosMedico.jpg)
![Crear Turno](vistas/CrearTurno.jpg)
![Ver Turno](vistas/VerTurno.jpg)
![Index Pacientes](vistas/IndexPacientes.jpg)
![Crear Paciente](vistas/CrearPaciente.jpg)
![Ver Paciente](vistas/VerPaciente.jpg)
![Editar Paciente](vistas/EditarPaciente.jpg)
![Turnos Calendario](vistas/TurnosCalendario.jpg)
![Index Parámetros](vistas/IndexParametros.jpg)
![Crear Barrio](vistas/CrearBarrio.jpg)
![Editar Barrio](vistas/EditarBarrio.jpg)
![Crear Obra Social](vistas/CrearObraSocial.jpg)
![Editar Obra Social](vistas/EditarObraSocial.jpg)
![Crear Tipo Atencion](vistas/CrearTipoAtencion.jpg)
![Editar Tipo Atencion](vistas/EditarTipoAtencion.jpg)
![Crear Tipo Telefono](vistas/CrearTipoTelefono.jpg)
![Editar Tipo Telefono](vistas/EditarTipoTelefono.jpg)

## Historias de Usuario

***
*Id:* 1

*Nombre:* Crear Turno

*Historia de Usuario:* Como Secretaria/o quiero crear un Turno para agendar los horarios de atención del Médico.
***

*Id:* 2

*Nombre:* Confirmar Turno

*Historia de Usuario:* Como Secretaria/o quiero confirmar un turno para informar las atenciones que se van a realizar.
***

*Id:* 3

*Nombre:* Filtrar Turnos por Fecha

*Historia de Usuario:* Como Secretaria/o quiero filtrar los turnos para informar las atenciones que se van a realizar en un rango de fechas.
***

*Id:* 4

*Nombre:* Crear Paciente

*Historia de Usuario:* Como Secretaria/o quiero crear un Paciente para registrar los datos personales.
***

*Id:* 5

*Nombre:* Crear Barrio

*Historia de Usuario:* Como Secretaria/o quiero crear un Barrio para asignarlo a un Paciente y conocer su ubicación en el caso de una Atención a domicilio.
***

*Id:* 6

*Nombre:* Crear Obra Social

*Historia de Usuario:* Como Secretaria/o quiero crear una Obra Social para asignarla a un Paciente y conocer su afiliación.
***

*Id:* 7

*Nombre:* Crear Tipo de Atención

*Historia de Usuario:* Como Secretaria/o quiero crear un Tipo de Atención para asignarlo a un Turno y organizar el día de trabajo del Médico y atenciones.
***

*Id:* 8

*Nombre:* Crear Tipo Teléfono

*Historia de Usuario:* Como Secretaria/o quiero crear un Tipo de Teléfono para asignarlo a un Paciente y poseer otra vía de comunicación.
***

*Id:* 9

*Nombre:* Editar Paciente

*Historia de Usuario:* Como Secretaria/o quiero editar un Paciente para modificar los datos y actualizar la información del mismo.
***

*Id:* 10

*Nombre:* Editar Barrio

*Historia de Usuario:* Como Secretaria/o quiero editar un Barrio para modificar los datos en el caso que haya algún error.
***

*Id:* 11

*Nombre:* Editar Obra Social

*Historia de Usuario:* Como Secretaria/o quiero editar una Obra Social para modificar los datos en el caso que haya algún error.
***

*Id:* 12

*Nombre:* Editar Tipo Teléfono

*Historia de Usuario:* Como Secretaria/o quiero editar un Tipo de Teléfono para modificar los datos en el caso que haya algún error.
***

*Id:* 13

*Nombre:* Editar Tipo Atención

*Historia de Usuario:* Como Secretaria/o quiero editar un Tipo de Atención para modificar los datos en el caso que haya algún error.
***

*Id:* 14

*Nombre:* Eliminar Obra Social

*Historia de Usuario:* Como Secretaria/o quiero eliminar una Obra Social para actualizar el listado de las Obras Sociales con las que trabaja el Médico.
***

*Id:* 15

*Nombre:* Filtrar Turnos por Fecha, Estado y Tipo de Atención

*Historia de Usuario:* Como Médico/o quiero filtrar los turnos para poder obtener un reporte con la lista de las atenciones en formato PDF.
***

*Id:* 16

*Nombre:* Envío de correo electrónico al Paciente

*Historia de Usuario: Como Cliente quiero que el sistema envíe un correo electrónico al paciente cuando se crea el Turno para proporcionarle un aviso con los datos del Turno (descripción, fecha y hora).
***

*Id:* 17

*Nombre:* Cancelar Turnos (manual)

*Historia de Usuario: Como Secretario quiero poder Cancelar un Turno (no Confirmado) antes de la fecha programada para liberar la reserva y poder asignarla a otro Paciente.
***

*Id:* 18

*Nombre:* Cancelar Turnos (automático)

*Historia de Usuario: Como Cliente quiero que el sistema cancele, un día antes de la fecha programada, los Turnos que no fueron confirmados para que el Médico tenga información de los turnos que no asistirán al Consultorio.
***







