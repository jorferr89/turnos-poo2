## Diseño orientado a objetos

![Diagrama de Clases](diagramaI5/DiagramaClases.jpg)

(*) No se realizaron cambios en el Diagrama de Clases 

## Backlog de iteración

* Como Cliente quiero que el sistema envíe un correo electrónico al paciente cuando se crea el Turno para proporcionarle un aviso con los datos del Turno (descripción, fecha y hora).
* Como Secretario quiero poder Cancelar un Turno (no Confirmado) antes de la fecha programada para liberar la reserva y poder asignarla a otro Paciente. 
* Como Cliente quiero que el sistema cancele, un día antes de la fecha programada, los Turnos que no fueron confirmados para que el Médico tenga información de los turnos que no asistirán al Consultorio.

## Tareas

* Investigar y realizar las configuraciones para trabajar con correos en Laravel.
* Agregar el envío de correo electrónico al método de "Guardar Turno".
* Crear el método para Cancelar Turno en el controlador.
* Crear la ruta.
* Crear un comando para realizar la cancelación de Turnos.