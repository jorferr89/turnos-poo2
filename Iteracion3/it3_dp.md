## Diseño orientado a objetos

![Diagrama de Clases](diagramaI3/DiagramaClases.jpg)

## Backlog de iteración

* Como Secretaria/o quiero editar un Paciente para modificar los datos y actualizar la información del mismo.
* Como Secretaria/o quiero editar un Barrio para modificar los datos en el caso que haya algún error.
* Como Secretaria/o quiero editar una Obra Social para modificar los datos en el caso que haya algún error. 
* Como Secretaria/o quiero eliminar una Obra Social para actualizar el listado de las Obras Sociales con las que trabaja el Médico.
* Como Secretaria/o quiero editar un Tipo de Atención para modificar los datos en el caso que haya algún error.
* Como Secretaria/o quiero editar un Tipo de Teléfono para modificar los datos en el caso que haya algún error.
* Como Secretaria/o quiero filtrar los turnos para informar las atenciones que se van a realizar en un rango de fechas.

## Tareas

* Crear los métodos en los controladores
* Crear rutas
* Crear vistas