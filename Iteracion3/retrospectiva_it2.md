## Retrospectiva Iteración 2

En la iteración 2 se implementaron las altas de las clases Barrio, Tipo de Atención, Obra Social y Tipo de Teléfono.

Se cumplieron todas las tareas programadas para ésta iteración.

El mayor inconveniente fué la investigación sobre cómo incluir un Calendario en el proyecto. Se optó por utilizar FullCalendar.

Para la tercera iteración, es importante incluir filtros de búsqueda para los Turnos.