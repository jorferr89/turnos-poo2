## Integrantes (Grupo 3)
* Rodríguez Jorge Fernando

## Objetivo 

El sistema permitirá administrar los Turnos asignados a los Pacientes de un Consultorio Médico. A cada Paciente se le asignará un Turno en una fecha, hora establecida y se le enviará un mail de aviso con los datos del mismo. Es importante que el Paciente se comunique con el/la Secretario/a para confirmar o cancelar el turno (el tiempo máximo de confirmación es de un día antes de la fecha programada), en el caso que no se confirme, el sistema modificará de manera automática el estado del turno a “cancelado”.

## Módulos

*Módulo de Turnos:* El turno será asignado por un/una secretario/a a un paciente. Los datos del turno son: descripción, fecha, hora, tipo de atención (en consultorio o a domicilio), estado (en espera, confirmado, cancelado).
Se podrán obtener informes sobre los turnos en un rango de fechas, por tipo de prestación, y por estado. 

*Módulo de Pacientes:* Antes de asignar un turno a un paciente, el mismo debe estar registrado en el sistema. Los datos del paciente son: apellidos, nombres, nro. de documento, obra social, sexo, nro. de teléfono y tipo de teléfono, correo electrónico, barrio y dirección. Los datos de barrio y dirección son importantes si la consulta es a domicilio.

*Modulo de Parámetros:* Los parámetros del sistema serán: Tipo de Atención, Obra Social, Barrio y Tipo Teléfono. El/la encargado/a de administrarlos será el/la secretario/a. Los Estados no serán parametrizables.


