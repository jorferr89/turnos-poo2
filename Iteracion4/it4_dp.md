## Diseño orientado a objetos

![Diagrama de Clases](diagramaI4/DiagramaClases.jpg)

(*) No se realizaron cambios en el Diagrama de Clases 

## Backlog de iteración

* Como Médico/o quiero filtrar los turnos para poder obtener un reporte con la lista de las atenciones en formato PDF.

## Tareas

* Agregar el código para poder realizar el filtrado en el método "Index" del controlador de Turno.