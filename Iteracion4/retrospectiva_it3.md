## Retrospectiva Iteración 3

En la iteración 3 se implementaron las modificaciones para los datos de Paciente, Barrio, Tipo de Atención, Obra Social, Tipo de Teléfono, y la eliminación (de forma lógica) de una Obra Social.

Se cumplieron todas las tareas programadas para ésta iteración.

Se realizaron la lecturas proporcionadas en la clase, no hubo inconvenientes.

Para la cuarta iteración, se incluirán filtros de búsqueda más avanzados para los Turnos (por fechas, por Estado, por Tipo de Atención).