## Diseño orientado a objetos

![Diagrama de Clases](diagramaI2/DiagramaClases.jpg)

## Backlog de iteración

* Como Secretaria/o quiero crear un Barrio para asignarlo a un Paciente y conocer su ubicación en el caso de una Atención a domicilio.
* Como Secretaria/o quiero crear una Obra Social para asignarla a un Paciente y conocer su afiliación.
* Como Secretaria/o quiero crear un Tipo de Atención para asignarlo a un Turno y organizar el día de trabajo del Médico y atenciones.
* Como Secretaria/o quiero crear un Tipo de Teléfono para asignarlo a un Paciente y poseer otra vía de comunicación.

## Tareas
* Crear las tablas barrios, obras sociales, tipos de atenciones y tipos de telefonos en la Base de Datos
* Crear los modelos Barrio, Obra Social, Tipo de Atención y Tipo de Teléfono con sus respectivos atributos y relaciones con las demás tablas
* Crear seeders para datos de prueba
* Crear controladores BarrioController, ObraSocialController, TipoAtencionController y TipoTelefonoController
* Crear los métodos en los controladores
* Crear rutas
* Crear vistas