## Retrospectiva Iteración 1

En la iteración 1 se realizó la configuración inicial del proyecto (descarga del framework y configuración de variables de entorno). También se comenzó a programar las altas de las clases principales (Turno y Paciente) y algunas funcionalidades.

Se cumplieron todas las tareas programadas para ésta iteración.

El mayor inconveniente fué la investigación y lectura sobre la metodología que se está ocupando, ya que en la carrera no utilizamos (de manera práctica) el prototipado.

Para la segunda iteración, es importante incluir un Calendario para que el/la secretario/a tenga una mejor forma de visualizar los turnos asignados. Se buscarán opciones.